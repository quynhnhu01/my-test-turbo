'use client'
import React, { useEffect, useRef } from 'react';

export const useDidMountEffect = (
  effectCallback: React.EffectCallback,
  deps: React.DependencyList | undefined,
): void => {
  const didMount = useRef(false);

  useEffect(() => {
    if (didMount.current) effectCallback();
    else didMount.current = true;
  }, deps);
};
