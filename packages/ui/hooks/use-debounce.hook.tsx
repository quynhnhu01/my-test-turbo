let timerId: NodeJS.Timeout;

export const useDebounce = (delay: number = 300) => {
  const handleSetDebounce = (callback: Function) => {
    clearTimeout(timerId);
    timerId = setTimeout(() => {
      callback();
    }, delay);
  };

  return handleSetDebounce;
};
