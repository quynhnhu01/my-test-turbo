'use client'
import { useState, useEffect } from "react";

const getWindowDimension = () => {
  return {
    width: window.innerWidth,
    height: window.innerHeight,
  };
};

export const useWindowDimensions = () => {
  const [windowDimensions, setWindowDimensions] = useState(() =>
    getWindowDimension()
  );

  useEffect(() => {
    const handleResize = () => {
      setWindowDimensions(getWindowDimension());
    };

    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return [windowDimensions.width, windowDimensions.height];
};
