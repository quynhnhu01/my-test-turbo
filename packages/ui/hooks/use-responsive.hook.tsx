'use client'
import { useState, useEffect } from 'react';
import { FlattenSimpleInterpolation, css, useTheme } from 'styled-components';

type ResponsiveStyles = {
  mobile: (args: TemplateStringsArray) => FlattenSimpleInterpolation;
  tablet: (args: TemplateStringsArray) => FlattenSimpleInterpolation;
  desktop: (args: TemplateStringsArray) => FlattenSimpleInterpolation;
  widescreen: (args: TemplateStringsArray) => FlattenSimpleInterpolation;
};

type useResponsiveProps = {
  mobile: number;
  tablet: number;
  desktop: number;
  widescreen: number;
};

export const useResponsive = ({
  mobile,
  tablet,
  desktop,
  widescreen,
}: useResponsiveProps): ResponsiveStyles => {
  const [_, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });

  const minWidths = {
    mobile: 0,
    tablet: mobile + 1,
    desktop: tablet + 1,
    widescreen: desktop + 1,
  };

  const maxWidths = {
    mobile,
    tablet,
    desktop,
    widescreen,
  };
  useEffect(() => {
    const handleResize = () => {
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    };

    handleResize();
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const getMediaQuery = (minWidth: number, maxWidth: number) => (args: TemplateStringsArray) => {
    return css`
      @media (min-width: ${minWidth}px) and (max-width: ${maxWidth}px) {
        ${css(args)};
      }
    `;
  };

  const media = {
    mobile: getMediaQuery(minWidths.mobile, maxWidths.mobile),
    tablet: getMediaQuery(minWidths.tablet, maxWidths.tablet),
    desktop: getMediaQuery(minWidths.desktop, maxWidths.desktop),
    widescreen: getMediaQuery(minWidths.widescreen, maxWidths.widescreen),
  };

  return media;
};
