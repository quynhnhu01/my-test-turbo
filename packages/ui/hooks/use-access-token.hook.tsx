'use client'
import { useEffect, useState } from "react";

export const useAccessToken = () => {
  const [accessToken, setAccessToken] = useState<string | null | undefined>();
  useEffect(() => {
    const authToken = localStorage.getItem("access_token");
    if (authToken) setAccessToken(authToken);
  });

  return [accessToken];
};
