import {styled, system } from "@xstyled/styled-components";
import { Button } from "antd";
import { LoadingSpinner } from "../loading";
import { ReactElement } from "react";
import { styleVariables } from "../../style";
import { RootButtonProps } from "./root-button";

export interface LinkButtonProps extends RootButtonProps {
  loading?: boolean;
}

const StyledLinkButton = styled(Button)`
  ${system};
  color: ${styleVariables.colors.blue3};
  font-size: 16px;
  background-color: ${styleVariables.colors.blue1};
  border-color: ${styleVariables.colors.blue1};
  border-radius: 2px;
  padding: 8px 15px;
  line-height: 24px;
  &:hover,
  &:focus {
    color: ${styleVariables.colors.blue3};
    border-color: none;
    background: none;
  }
`;

export const LinkButton = (props: LinkButtonProps): ReactElement => {
  const { loading, children } = props;

  return (
    <StyledLinkButton {...props} type="link" loading={false}>
      {loading ? <LoadingSpinner primary /> : children}
    </StyledLinkButton>
  );
};
