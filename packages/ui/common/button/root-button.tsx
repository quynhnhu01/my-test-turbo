import { ReactElement } from "react";
import { Button } from "antd";
import { styled, system } from "@xstyled/styled-components";
import { NativeButtonProps } from "antd/lib/button/button";

export interface RootButtonProps extends NativeButtonProps {}
const StyledButton = styled(Button) <RootButtonProps>`
  ${system};
  outline: none;
  font-size: ${props => props.size === "large" ? "16px" : "14px"};
  border-radius: 2px;
  padding: ${props => props.size === "large" ? "8px" : "4px"} 15px;
  line-height: 24px;
`;

export const RootButton = (props: RootButtonProps): ReactElement => {
  const { children, ...rest } = props;
  return (
    <StyledButton
      {...rest}
    >
      {children}
    </StyledButton>
  );
};
