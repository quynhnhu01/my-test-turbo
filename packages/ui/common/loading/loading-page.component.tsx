import { ReactElement } from 'react';
import { styleVariables } from '../../style';
import { styled } from '@xstyled/styled-components';

interface LoadingPageProps {
  title?: string;
  size?: 'middle' | 'large';
}

const StyledLoadingPage = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100%;
`;
const StyledTitle = styled.div<LoadingPageProps>`
  font-size: ${(props) => (props.size === 'large' ? '25px' : '20px')};
  font-weight: 500;
`;
const StyledLoading = styled.div<LoadingPageProps>`
  span {
    height: ${(props) => (props.size === 'large' ? '25px' : '20px')};
    width: ${(props) => (props.size === 'large' ? '25px' : '20px')};
    margin: 0 5px;
    background-color: ${styleVariables.colors.blue4};
    border-radius: 50%;
    display: inline-block;
    animation-name: dots;
    animation-duration: 2s;
    animation-iteration-count: infinite;
    animation-timing-function: ease-in-out;
    &:nth-child(2) {
      animation-delay: 0.4s;
    }
    &:nth-child(3) {
      animation-delay: 0.8s;
    }
  }
  @keyframes dots {
    50% {
      transform: scale(0.7) translateY(10px);
    }
  }
`;
export const LoadingPage = ({
  title,
  size = 'large',
}: LoadingPageProps): ReactElement => {
  return (
    <StyledLoadingPage>
      <StyledTitle size={size}>{title || ''}</StyledTitle>
      <br />
      <StyledLoading size={size}>
        <span></span>
        <span></span>
        <span></span>
      </StyledLoading>
    </StyledLoadingPage>
  );
};
