import { Skeleton, SkeletonProps } from "antd";
import React, { ReactElement } from "react";

export interface NormalSkeletonLoadingProps extends SkeletonProps {}

export const NormalSkeletonLoading = (
  props: NormalSkeletonLoadingProps
): ReactElement => {
  const { active } = props;

  return <Skeleton {...props} active={active ?? true} />;
};
