import { ReactElement } from "react";
import { Skeleton } from "antd";
import { AvatarProps } from "antd/lib/skeleton/Avatar";

export interface AvatarSkeletonLoadingProps extends AvatarProps {}

export const AvatarSkeletonLoading = (
  props: AvatarSkeletonLoadingProps
): ReactElement => {
  return <Skeleton.Avatar {...props} />;
};
