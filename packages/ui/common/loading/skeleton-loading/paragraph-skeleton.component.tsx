import { Skeleton } from "antd";
import { SkeletonParagraphProps } from "antd/lib/skeleton/Paragraph";
import { ReactElement } from "react";

export interface ParagraphSkeletonLoadingProps extends SkeletonParagraphProps {}

export const ParagraphSkeletonLoading = (
  props: ParagraphSkeletonLoadingProps
): ReactElement => {
  return <Skeleton {...props} active />;
};
