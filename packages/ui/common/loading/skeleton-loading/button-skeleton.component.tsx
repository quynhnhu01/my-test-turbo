import { Skeleton } from "antd";
import { SkeletonButtonProps } from "antd/lib/skeleton/Button";
import React, { ReactElement } from "react";

export interface ButtonSkeletonLoadingProps extends SkeletonButtonProps {}

export const ButtonSkeletonLoading = (
  props: ButtonSkeletonLoadingProps
): ReactElement => {
  const { size } = props;
  return <Skeleton.Button {...props} size={size ?? "large"} />;
};
