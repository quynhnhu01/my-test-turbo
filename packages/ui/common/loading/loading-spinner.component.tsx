import { styled } from "@xstyled/styled-components";
import { styleVariables } from "../../style";

interface LoadingSpinnerProps {
  primary?: boolean;
}
const StyledLoadingSpinner = styled.div<LoadingSpinnerProps>`
  font-size: 27px;
  position: relative;
  display: inline-block;
  width: 1em;
  height: 1em;
  div {
    position: absolute;
    left: 0.4629em;
    bottom: 0;
    width: 0.074em;
    height: 0.2777em;
    border-radius: 0.0555em;
    background-color: transparent;
    -webkit-transform-origin: center -0.2222em;
    -ms-transform-origin: center -0.2222em;
    transform-origin: center -0.2222em;
    -webkit-animation: lds-spinner 1s infinite linear;
    animation: lds-spinner 1s infinite linear;
  }
  div:after {
    content: " ";
    display: block;
    position: absolute;
    width: 2px;
    height: 5px;
    border-radius: 40%;
    background: ${(props) =>
      props.primary
        ? styleVariables.colors.blue3
        : styleVariables.colors.gray1};
  }
  div:nth-child(1) {
    transform: rotate(0deg);
    animation-delay: -1.1s;
  }
  div:nth-child(2) {
    transform: rotate(45deg);
    animation-delay: -0.9s;
  }
  div:nth-child(3) {
    transform: rotate(90deg);
    animation-delay: -0.8s;
  }
  div:nth-child(4) {
    transform: rotate(135deg);
    animation-delay: -0.7s;
  }
  div:nth-child(5) {
    transform: rotate(180deg);
    animation-delay: -0.5s;
  }
  div:nth-child(6) {
    transform: rotate(225deg);
    animation-delay: -0.4s;
  }
  div:nth-child(7) {
    transform: rotate(270deg);
    animation-delay: -0.3s;
  }
  div:nth-child(8) {
    transform: rotate(315deg);
    animation-delay: -0.2s;
  }
  div:nth-child(9) {
    transform: rotate(360deg);
    animation-delay: 0s;
  }
  @keyframes lds-spinner {
    0% {
      opacity: 1;
    }
    100% {
      opacity: 0;
    }
  }
`;
export const LoadingSpinner = (props: LoadingSpinnerProps) => {
  return (
    <StyledLoadingSpinner {...props}>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </StyledLoadingSpinner>
  );
};
