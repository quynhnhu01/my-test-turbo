const { i18n } = require("./next-i18next.config");

const NEXTJS_IGNORE_ESLINT = process.env.NEXTJS_IGNORE_ESLINT === "1" || false;
const NEXTJS_IGNORE_TYPECHECK =
  process.env.NEXTJS_IGNORE_TYPECHECK === "1" || false;

const isProd = process.env.NODE_ENV === "production";

// Nestjs lazyImport
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const webpack = require("webpack");
const WebPackIgnorePlugin = {
  checkResource: function (resource) {
    const lazyImports = [
      "@nestjs/microservices",
      "@nestjs/microservices/microservices-module",
      "cache-manager",
      "class-transformer",
      "class-validator",
      "fastify-static",
      "@nestjs/websockets/socket-module",
      "@nestjs/platform-express",
    ];

    if (!lazyImports.includes(resource)) return false;

    try {
      require.resolve(resource);
    } catch (err) {
      return true;
    }

    return false;
  },
};

// Tell webpack to compile those packages
// @link https://www.npmjs.com/package/next-transpile-modules
const tmModules = [

];

/**
 * A way to allow CI optimization when the build done there is not used
 * to deliver an image or deploy the files.
 * @link https://nextjs.org/docs/advanced-features/source-maps
 */
const disableSourceMaps = process.env.NEXT_DISABLE_SOURCEMAPS === "true";
if (disableSourceMaps) {
  console.info(
    `${pc.green(
      "notice"
    )}- Sourcemaps generation have been disabled through NEXT_DISABLE_SOURCEMAPS`
  );
}

// Example of setting up secure headers
// @link https://github.com/jagaapple/next-secure-headers
const { createSecureHeaders } = require("next-secure-headers");
const secureHeaders = createSecureHeaders({
  contentSecurityPolicy: {
    directives: {
      //defaultSrc: "'self'",
      //styleSrc: ["'self'"],
    },
  },
  ...(isProd
    ? {
      forceHTTPSRedirect: [
        true,
        { maxAge: 60 * 60 * 24 * 4, includeSubDomains: true },
      ],
    }
    : {}),
  referrerPolicy: "same-origin",
});

/**
 * @type {import('next').NextConfig}
 */
const nextConfig = {
  reactStrictMode: true,
  productionBrowserSourceMaps: !disableSourceMaps,
  i18n,
  optimizeFonts: true,

  httpAgentOptions: {
    // @link https://nextjs.org/blog/next-11-1#builds--data-fetching
    keepAlive: true,
  },

  // @link https://nextjs.org/docs/advanced-features/output-file-tracing
  outputFileTracing: true,

  experimental: {
    // Prefer loading of ES Modules over CommonJS
    // @link {https://nextjs.org/blog/next-11-1#es-modules-support|Blog 11.1.0}
    // @link {https://github.com/vercel/next.js/discussions/27876|Discussion}
    esmExternals: false,
    // Experimental monorepo support
    // @link {https://github.com/vercel/next.js/pull/22867|Original PR}
    // @link {https://github.com/vercel/next.js/discussions/26420|Discussion}
    externalDir: true,
    nftTracing: true,
  },
  future: {
    // @link https://github.com/vercel/next.js/pull/20914
    strictPostcssConfiguration: true,
  },

  // @link https://nextjs.org/docs/basic-features/image-optimization
  images: {
    loader: "default",
    deviceSizes: [640, 750, 828, 1080, 1200, 1920, 2048, 3840],
    imageSizes: [16, 32, 48, 64, 96, 128, 256, 384],
    disableStaticImages: false,
    // https://nextjs.org/docs/api-reference/next/image#caching-behavior
    minimumCacheTTL: 60,
    // Allowed domains for next/image
    domains: ["source.unsplash.com"],
  },

  typescript: {
    ignoreBuildErrors: NEXTJS_IGNORE_TYPECHECK,
  },

  eslint: {
    ignoreDuringBuilds: NEXTJS_IGNORE_ESLINT,
    dirs: ["src"],
  },

  async headers() {
    return [{ source: "/(.*)", headers: secureHeaders }];
  },

  webpack: (config, { isServer }) => {
    if (!isServer) {
      // Swap sentry/node by sentry/browser
      config.resolve.alias["@sentry/node"] = "@sentry/browser";
    }
    // config.experiments = {
    //   topLevelAwait: true
    // };
    config.plugins.push(
      new CleanWebpackPlugin(),
      new webpack.IgnorePlugin(WebPackIgnorePlugin)
    );
    config.module.rules.push({
      test: /\.svg$/,
      issuer: /\.(js|ts)x?$/,
      use: [
        {
          loader: "@svgr/webpack",
          // https://react-svgr.com/docs/webpack/#passing-options
          options: {
            svgo: true,
            // @link https://github.com/svg/svgo#configuration
            svgoConfig: {
              multipass: false,
              datauri: "base64",
              js2svg: {
                indent: 2,
                pretty: false,
              },
            },
          },
        },
      ],
    });

    return config;
  },
  serverRuntimeConfig: {
    PROJECT_ROOT: __dirname,
    credential: {
      googleService: {
        clientId: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      },
    },
  },
  publicRuntimeConfig: {
    api: {
      baseUrl: process.env.API_BASE_URL_PATH,
      timeout: 10000,
    },
    page: {
      signIn: {
        url: `/sign-in`,
      },
      signUp: {
        url: `/sign-up`,
      },
      forgotPassword: {
        confirmEmail: {
          url: `/forgot-password`,
        },
        validateEmailConfirmation: {
          url: `/validate-email-confirmation`,
        },
        notFoundEmail: {
          url: `/not-found-email`,
        }
      },
      requestEmail: {
        url: `/request-email`,
      },
    },
    app: {
      identity: {
        baseUrl: process.env.APP_BASE_URL
      },
      teacher: {
        clientId: process.env.TEACHER_CLIENT_ID,
        clientSecret: process.env.TEACHER_CLIENT_SECRET,
        page: {
          callback: { url: `${process.env.TEACHER_APP_URL_PATH}/callback` },
        },
      },
      student: {
        clientId: process.env.STUDENT_CLIENT_ID,
        clientSecret: process.env.STUDENT_CLIENT_SECRET,
        page: {
          callback: { url: `${process.env.STUDENT_APP_URL_PATH}/callback` },
        },
      },
    },
    env: {
      APP_NAME: process.env.APP_NAME,
      ENVIRONMENT: process.env.ENVIRONMENT,
      BASE_URL: process.env.BASE,
      LANGUAGE: process.env.LANGUAGE,
      CLARITY_PROJECT_ID: process.env.CLARITY_PROJECT_ID,
    },
  },

};

let config;

if (tmModules.length > 0) {
  const withNextTranspileModules = require("next-transpile-modules")(
    tmModules,
    {
      resolveSymlinks: true,
      debug: false,
    }
  );
  config = withNextTranspileModules(nextConfig);
} else {
  config = nextConfig;
}

if (process.env.ANALYZE === "true") {
  // @ts-ignore
  const withBundleAnalyzer = require("@next/bundle-analyzer")({
    enabled: true,
  });
  config = withBundleAnalyzer(config);
}

module.exports = config;
